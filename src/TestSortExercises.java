/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author ehazizi
 */
public class TestSortExercises {

    public static void main(String[] args) {
//        Integer[] a = new Integer[13];
        Integer[] a = new Integer[]{81, 94, 11, 96, 12, 35, 17, 95, 28, 58, 41, 75, 15};

        System.out.print("Printing initial state of arrray" + "\n");
        for (int printArrState = 0; printArrState < a.length; printArrState++) {
            System.out.print(" " + a[printArrState]);

        }
        System.out.println();
        Sort.insertionSort(a);
        Integer[] b = new Integer[]{81, 94, 11, 96, 12, 35, 17, 95, 28, 58, 41, 75, 15};
        System.out.println();
        System.out.print("Printing initial state of arrray" + "\n");
        for (int printArrState = 0; printArrState < b.length; printArrState++) {
            System.out.print(" " + b[printArrState]);

        }
        System.out.println();
        Sort.shellsort(b);
        Integer[] c = new Integer[]{81, 94, 11, 96, 12, 35, 17, 95, 28, 58, 41, 75, 15};
        System.out.println();
        System.out.print("Printing initial state of arrray" + "\n");
        for (int printArrState = 0; printArrState < c.length; printArrState++) {
            System.out.print(" " + c[printArrState]);

        }
        System.out.println();
        Sort.shellsort2(c);
        Integer[] d = new Integer[]{81, 94, 11, 96, 12, 35, 17, 95, 28, 58, 41, 75, 15};
        System.out.println();
        System.out.print("Printing initial state of arrray" + "\n");
        for (int printArrState = 0; printArrState < d.length; printArrState++) {
            System.out.print(" " + d[printArrState]);

        }
        System.out.println();
        Sort.selectionSort(d);
        
        Integer[] e = new Integer[]{6, 5,3,1,8,7,2,4};
        System.out.println();
        System.out.print("Printing initial state of arrray before mergeSort" + "\n");
        for (int printArrState = 0; printArrState < e.length; printArrState++) {
            System.out.print(" " + e[printArrState]);

        }
        System.out.println();
        Sort.mergeSort2(e);
    }
}
