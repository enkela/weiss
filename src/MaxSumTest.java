/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author CS
 */

import java.util.Random;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author CS
 */
public class MaxSumTest {

    static private int seqStart = 0;
    static private int seqEnd = 1;

    public static int maxSubSeq1(int[] a) {
        int maxSum = 0;
        for (int i = 0; i < a.length; i++) {
            for (int j = i; j < a.length; j++) {
                int thisSum = 0;
                for (int k = i; k <= j; k++) {
                    thisSum += a[k];
                    if (thisSum > maxSum) {
                        maxSum = thisSum;
                        seqStart = i;
                        seqEnd = j;
                    }

                }

            }

        }
        return maxSum;

    }

    //This is the quadratic method
    public static int maxSubSeq2(int[] a) {
        int maxSum = 0;
        for (int i = 0; i < a.length; i++) {
            for (int j = i; j < a.length; j++) {
                int thisSum = 0;
                for (int k = i; k <= j; k++) {
                    thisSum += a[k];
                    if (thisSum > maxSum) {
                        maxSum = thisSum;
                        seqStart = i;
                        seqEnd = j;
                    }

                }

            }

        }
        return maxSum;

    }

    //This is the linear method
    public static int maxSubSeq3(int[] a) {
        int maxSum = 0;
        System.out.println("Current Time is:" + System.currentTimeMillis());
        for (int i = 0; i < a.length; i++) {
            for (int j = i; j < a.length; j++) {
                int thisSum = 0;
                for (int k = i; k <= j; k++) {
                    thisSum += a[k];
                    if (thisSum > maxSum) {
                        maxSum = thisSum;
                        seqStart = i;
                        seqEnd = j;
                    }

                }

            }

        }
        return maxSum;

    }

    /**
     * @param args the command line arguments
     */
    private static Random rand = new Random();

    public static void main(String[] args) {
        long beginTime ;
        int a[] = {4, -3, 5, -2, 2};
        int n = 4000;
        int[] b = new int[n];
        for (int i = 0; i < n; i++) {
            b[i] = rand.nextInt(100) - 30;
        }
        int maxSum;
        beginTime = System.currentTimeMillis();
        maxSum = maxSubSeq1(a);
        System.out.println("MaxSumTest.main() is displaying as MAXSUM: " + maxSum
                + "\nseqStart: " + seqStart + "\nseqEnd: " + seqEnd);
        System.out.println("Current Time difference after the alg is:" + (System.currentTimeMillis() - beginTime));
        System.out.println("Executing the same alg for array b with 1000 el");
        beginTime = System.currentTimeMillis();
        int maxSum2 = maxSubSeq1(b);
        System.out.println("Current Time difference after the alg is:" + (System.currentTimeMillis() - beginTime));

    }

}

