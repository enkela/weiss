
import weiss.util.Comparator;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author Server Lab1
 */
public class AutoComparator<Auto> implements Comparator<Auto> {

    @Override
    public int compare(Auto lhs, Auto rhs) {
        return lhs.toString().compareTo(rhs.toString());
    }

}
