
import weiss.nonstandard.LinkedList;
import weiss.nonstandard.LinkedListIterator;







/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Server Lab1
 */



/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Server Lab1
 */
public class TestProgramDouble {

    public static void main(String[] args) {
//        Auto[] autos = new Auto[5];
        Auto a1 = new Auto("W", "red");

        Auto a2 = new Auto("B", "red");
        Auto a3 = new Auto("M", "red");
        Auto a4 = new Auto("F", "yellow");
        LinkedList<Auto> myAutos= new LinkedList<Auto>();
        LinkedListIterator<Auto> itr= myAutos.zeroth();
        myAutos.insert(a4,itr);
        myAutos.insert(a1,itr);
        myAutos.insert(a3,itr);
        myAutos.insert(a4,itr);
//        myAutos.addFirst(a2);
        System.out.println(" First View: "+myAutos );
        LinkedList.printList(myAutos);
        
        System.out.println(" Print the last element" );
        myAutos.findLast(a4);
        System.out.println(" Print the new linked list after removing all a4 object" );
        myAutos.removeAll(a4);
        LinkedList.printList(myAutos);
        
        System.out.println(" Print the new linked list after cloning the old" );
        LinkedList<Auto> cloneAutos= myAutos.clone();
        LinkedList.printList(cloneAutos);
        System.out.println(" Print the minimum element in linked listcomparing by NameOfAuto" );
        Auto a=myAutos.findMinimum(new AutoComparator<Auto>());
        System.out.println(" Print the minimum element: " +a.toString());
        
        
        
        
    }

}

