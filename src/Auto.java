/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author ehazizi
 */
public class Auto {

    private String nameAuto;
    private String color;
    private Engine e;

    public Auto(String nameAuto, String color) {
        this.nameAuto = nameAuto;
        this.color = color;
    }

    public String getNameAuto() {
        return nameAuto;
    }

    public void setNameAuto(String nameAuto) {
        this.nameAuto = nameAuto;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public Engine getE() {
        return e;
    }

    public void setE(Engine e) {
        this.e = e;
    }

    @Override
    public String toString() {
        return "Auto{" + "nameAuto=" + nameAuto + ", color=" + color + ", e=" + e + '}';
    }
    

}
