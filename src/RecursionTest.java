/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author ehazizi
 */
public class RecursionTest {
//Long variable is holding 16 bit or 2 byte

    public static int s(int n) {
        if (n == 1) {
            return 1;
        } else {
            return s(n - 1) + n;
        }
    }
///fourth rule

    public static long fib(int n) {
        if (n <= 1) {
            return n;

        } else {
            return fib(n - 1) + fib(n - 2);
        }

    }

    public static long fibRight(int n) {
        long prev = 0, next = 1, result = 0;
        for (int i = 0; i < n; i++) {
            result = prev + next;
            prev = next;
            next = result;
        }
        return result;

    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        System.out.println("Print max: " + Integer.MAX_VALUE);
        System.out.println("Print result: " + s(10000));
        System.out.println("Print result of fib: " + fibRight(4));
    }

}
